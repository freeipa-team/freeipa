#!/usr/bin/make -f 

# Uncomment this to turn on verbose mode.
#export DH_VERBOSE=1

include /usr/share/dpkg/architecture.mk
include /usr/share/dpkg/pkg-info.mk

CFLAGS += -D_FORTIFY_SOURCE=2

# build server only where nodejs is available
#ifneq (,$(filter $(DEB_HOST_ARCH), amd64 arm64 armhf i386 mips mips64el mipsel ppc64 ppc64el s390x))
#  ONLY_CLIENT=0
#else
  ONLY_CLIENT=1
#endif

DESTDIR=$(CURDIR)/debian/tmp

export PYTHON=python3
export am_cv_python_pythondir=/usr/lib/python3/dist-packages
export am_cv_python_pyexecdir=/usr/lib/python3/dist-packages
export systemdsystemunitdir=$(shell pkgconf --variable=systemdsystemunitdir systemd | sed s,^/,,)

confflags = \
	--libexecdir=/usr/lib/ \
	--with-ipaplatform=debian \
	--with-sysconfenvdir=/etc/default \
	--disable-pylint \
	--without-ipa-join-xml \
	--without-jslint

ifeq ($(ONLY_CLIENT), 1)
confflags += --disable-server \
	--without-ipatests
else
confflags += --enable-server \
	--with-ipatests
endif

# For maintainer use only, generate a tarball:
gentarball:
	tar --transform 's,^,$(DEB_SOURCE)-$(DEB_VERSION_UPSTREAM)/,' \
		--exclude './debian' --exclude-vcs \
		-cJf ../$(DEB_SOURCE)_$(DEB_VERSION_UPSTREAM).orig.tar.xz .

gencontrol:
ifeq ($(ONLY_CLIENT), 1)
	cat debian/control.stub \
	debian/control.common \
	> debian/control
else
	cat debian/control.stub \
	debian/control.server-bdeps \
	debian/control.common \
	debian/control.server \
	> debian/control
endif

override_dh_clean: gencontrol
	dh_clean
	rm -f po/*.gmo po/Makefile.hack po/POTFILES.in po/stamp-po
	rm -rf contrib/completion/ipa.bash_completion daemons/ipa-version.h \
	       install/ui/src/libs/loader.js m4
	rm -f ipaplatform/override.py ipapython/version.py ipasetup.py

override_dh_auto_configure: gencontrol
	dh_auto_configure -- $(confflags)

# tests would just fail, they need a proper environment with 389 running et al
override_dh_auto_test:

override_dh_auto_install:
	dh_auto_install --max-parallel=1

ifneq ($(ONLY_CLIENT), 1)
	chmod 755 $(DESTDIR)/usr/lib/ipa/certmonger/*

	touch $(DESTDIR)/usr/share/ipa/html/ca.crt
	touch $(DESTDIR)/usr/share/ipa/html/kerberosauth.xpi
	touch $(DESTDIR)/usr/share/ipa/html/krb.con
	touch $(DESTDIR)/usr/share/ipa/html/krb.js
	touch $(DESTDIR)/usr/share/ipa/html/krb5.ini
	touch $(DESTDIR)/usr/share/ipa/html/krbrealm.con
endif

	mkdir -p $(DESTDIR)/usr/share/bash-completion/completions
	mv $(DESTDIR)/etc/bash_completion.d/ipa $(DESTDIR)/usr/share/bash-completion/completions

	# purge .la files
	find $(CURDIR)/debian/tmp -name "*.la" -type f -exec rm -f "{}" \;
	# purge precompiled .pyc/.pyo files
	find $(CURDIR)/debian/tmp -name '*.py[c,o]' -exec rm '{}' ';'
	# fix permissions
	find $(CURDIR)/debian/tmp -name "*.mo" -type f -exec chmod -x "{}" \;
	# remove files which are useful only for make uninstall
	find $(CURDIR)/debian/tmp -wholename '*/dist-packages/*/install_files.txt' -exec rm '{}' \;
	# fix the interpreter
	find debian/tmp ! -name '*.pyc' -a ! -name '*.pyo' -a \
	    -type f -exec grep -qsm1 '^#!.*\bpython' {} \; \
	    -exec sed -i -e '1 s|^#!.*\bpython[^ ]*|#!/usr/bin/python3|' {} \;
ifeq ($(ONLY_CLIENT), 0)
	# remove ipasphinx for now
	rm -r $(CURDIR)/debian/tmp/usr/lib/python3/dist-packages/ipasphinx*
endif

override_dh_missing:
	dh_missing --fail-missing

ifneq ($(ONLY_CLIENT), 1)
override_dh_installsystemd:
	dh_installsystemd -pfreeipa-server --no-enable ipa.service
	dh_installsystemd -pfreeipa-server --no-enable ipa-dnskeysyncd.service
	dh_installsystemd -pfreeipa-server --no-enable ipa-custodia.service
	dh_installsystemd -pfreeipa-server --no-enable ipa-ods-exporter.service
endif

override_dh_fixperms:
	dh_fixperms

	# check needed to not fail arch-indep build which doesn't run dh_installdirs
	if [ -d $(CURDIR)/debian/freeipa-server/etc/ipa/custodia ]; then \
	    chmod 0700 $(CURDIR)/debian/freeipa-server/etc/ipa/custodia; \
	    chmod 0700 $(CURDIR)/debian/freeipa-server/var/lib/ipa/backup; \
	fi

override_dh_gencontrol:
	dh_gencontrol -- \
	  -Vlib:Depends=$(shell dpkg-query -W -f '$${Depends}' librpm-dev \
			| sed -E 's/.*(librpm[[:alnum:].-]+).*/\1/')

%:
	dh $@ --with python3
#		--builddirectory=build
